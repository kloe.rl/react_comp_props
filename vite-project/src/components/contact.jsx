

export default function Contact({person, status}) {
    function IsConnected({status}) {
        if (status) {
            return <div className="connectedCircle green"></div>
        }
        return <div className="connectedCircle red"></div>
    }
    return (
        <section className="Contact">
            <h3>
                {person.name}
            </h3>
            <img
                src={person.imageUrl}
                alt={person.name}
                width= {200}
                style ={{ borderRadius :'50%'}}
            />
            <div>
                <IsConnected 
                    status={status}
                />
            </div>
        </section>
    );
}
