// import { useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
import './components/contact.css'
import './App.css'
import Contact from './components/contact.jsx'

function Profile() {
  return (
      <section className='contact__container'>
          <Contact 
              person={{name: 'MASAKI GITSUNE', imageUrl:'https://yokai.com/wordpress/wp-content/uploads/2021/12/masakigitsune-436x436.jpg'}}
              status={false}
          />
          <Contact 
              person={{name: 'NINGYO', imageUrl:'https://yokai.com/wordpress/wp-content/uploads/2018/03/ningyo-436x436.jpg'}}
              status={true}
          />
          <Contact 
              person={{name: 'RYÛTÔ', imageUrl:'https://yokai.com/wordpress/wp-content/uploads/2018/03/ryuutou-436x436.jpg'}}
              status={true}
          />
      </section>
  );
}

export default Profile